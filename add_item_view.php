<!DOCTYPE html>
<html>
<head>
    <title>My Guitar Shop</title>
    <link rel="stylesheet" type="text/css" href="main.css">
</head>
<body>
    <header>
        <h1>My Guitar Shop</h1>
    </header>
    <main>
        <h1>Add Item</h1>
        <form action="." method="post">
            <!-- this sends the key-value pair action=add"
                to the controller (see line 42 in index.php) -->
            <input type="hidden" name="action" value="add">

            <label>Name:</label>
            <select name="productkey">
            <!-- per p. 325 in the book, we store the $key
            from each $product in $products --> 
            <?php foreach($products as $key => $product) :
                $cost = number_format($product['cost'], 2);
                $name = $product['name'];
                $item = $name . ' ($' . $cost . ')';
            ?>
            <!-- the $key value is what is passed as
                "productkey" when the form is submitted --> 
                <option value="<?php echo $key; ?>">
                    <?php echo $item; ?>
                </option>
            <?php endforeach; ?>
                
            </select><br>

            <label>Quantity:</label>
            <select name="itemqty">
            <?php for($i = 1; $i <= 10; $i++) : ?>
                <!-- the SELECTED value of $i is what
                is passed as the value of "itemqty" --> 
                <option value="<?php echo $i; ?>">
                    <?php echo $i; ?>
                </option>
            <?php endfor; ?>
            </select><br>

            <label>&nbsp;</label>
            <input type="submit" value="Add Item">
        </form>
        <!-- this is where we send "action=show_cart" to the controller,
             via a GET request, to line 58 of index.php -->
        <p><a href=".?action=show_cart">View Cart</a></p>    
    </main>
</body>
</html>