<!-- 
This file is the CONTROLLER of the shopping cart application 
--> 
<?php
// Start session management with a persistent cookie
$lifetime = 60 * 60 * 24 * 14;    // 2 weeks in seconds
session_set_cookie_params($lifetime, '/');
session_start();

// Create a cart array if needed
// (Create a cart array if empty -- 'cart12' just 
// means a cart for the Chapter 12 application 
if (empty($_SESSION['cart12'])) { $_SESSION['cart12'] = array(); }

// Create a multi-dimensional array to store our table of products
// (In a real world app, this info would be read in from a database,
// but for the sake of simplicity, we're hard coding the product table
// as an array
// Create a table of products
$products = array();
$products['MMS-1754'] = array('name' => 'Flute', 'cost' => '149.50');
$products['MMS-6289'] = array('name' => 'Trumpet', 'cost' => '199.50');
$products['MMS-3408'] = array('name' => 'Clarinet', 'cost' => '299.50');

// Include cart functions (we are loading all of the MODEL functions 
// here, if they have not already been loaded
require_once('cart.php');

// Get the action to perform; default action here is to "show_add_item"
// Whatever the action is, this will dictate how the 
// switch statement works below
$action = filter_input(INPUT_POST, 'action');
if ($action === NULL) {
    $action = filter_input(INPUT_GET, 'action');
    if ($action === NULL) {
        $action = 'show_add_item';
    }
}

// Add or update cart as needed
// Could have used multi way if else statement as well
switch($action) {
    case 'add': // <-- Get add from "add_item_view.php"
        $product_key = filter_input(INPUT_POST, 'productkey');
        $item_qty = filter_input(INPUT_POST, 'itemqty');
        add_item($product_key, $item_qty);
        include('cart_view.php');
        break;
    case 'update': // <-- we get 'update' from cart_view.php
        $new_qty_list = filter_input(INPUT_POST, 'newqty', FILTER_DEFAULT, 
                                     FILTER_REQUIRE_ARRAY);
        foreach($new_qty_list as $key => $qty) {
            if ($_SESSION['cart12'][$key]['qty'] != $qty) {
                update_item($key, $qty);
            }
        }
        include('cart_view.php');
        break;
    case 'show_cart': // we get 'show_cart' from clicking the "View Cart" Link
                        // in add_item_view.php
        include('cart_view.php');
        break;
    case 'show_add_item': // <-- we get show_add_item from clicking the "Add Item"
                          // link in cart_view.php
        include('add_item_view.php');
        break;
    case 'empty_cart': // we get 'empty_cart' from clicking the "Empty Cart" 
                       // link in cart_view.php
        unset($_SESSION['cart12']);
        include('cart_view.php');
        break;
}
?>